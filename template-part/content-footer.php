<footer class="bg-gray-400">
    <div class="container mx-auto py-12 px-4 lg:px-0">
        <a href="<?php echo home_url(); ?>" class="h-9 w-52 mb-12">
            <img src="<?php the_field('logo', 'option'); ?>" alt="logo"
                class="h-9 w-52 mb-12">
        </a>
        <div class="grid lg:grid-cols-2 grid-cols-1 gap-8">
            <div class="flex flex-col">
                <div class="text-xl mb-5 text-gray-100">درباره ما</div>
                <div class="w-80 text-gray-200 leading-7">
                    <?php the_field('about', 'option'); ?>
                </div>
            </div>
            <div class="grid lg:grid-cols-2 grid-cols-1 gap-20">
                <div class="flex flex-col">
                    <div class="text-xl mb-3.5 text-gray-100">اطلاعات تماس</div>
                    <div class="gap-y-3 grid text-gray-200">
                        <div><?php the_field('contact_num_1', 'option'); ?></div>
                        <div><?php the_field('contact_num_2', 'option'); ?></div>
                        <div class="leading-8">
                        <?php the_field('address', 'option'); ?>
                        </div>
                    </div>
                </div>
                <div class="flex flex-col">
                    <div class="text-xl mb-3.5 text-gray-100">دسترسی سریع</div>
                    <div class="gap-y-3 grid text-gray-200">
                    <?php if( have_rows('links_footer', 'option') ): ?>
                    <?php while( have_rows('links_footer', 'option') ): the_row(); ?>
                    <a href="#" class="hover:text-primary-100"><?php the_sub_field('links', 'option'); ?></a>
                    <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="bg-gray-500 py-6">
    <div class="container mx-auto">
        <div class="grid grid-cols-1 lg:grid-cols-2 gap-6">
            <div class="flex gap-4 justify-center lg:justify-start">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/social/telegram.svg" alt="telegram"
                    class="h-6 w-6 grayscale opacity-50 hover:opacity-100 hover:grayscale-0">
                </a>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/social/facebook.svg" alt="facebook"
                    class="h-6 w-6 grayscale opacity-50 hover:opacity-100 hover:grayscale-0">
                </a>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/social/whatsapp.svg" alt="whatsapp"
                    class="h-6 w-6 grayscale opacity-50 hover:opacity-100 hover:grayscale-0">
                </a>
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/social/instagram.svg" alt="instagram"
                    class="h-6 w-6 grayscale opacity-50 hover:opacity-100 hover:grayscale-0">
                </a>
            </div>
            <div class="dir-ltr text-gray-300 lg:text-left text-center">
            ©2020 - company  |   All rights reserved
            </div>
        </div>
    </div>
</div>