<div dir="ltr" class="swiper mySwiper">
    <div class="swiper-wrapper">
    <?php if( have_rows('slider', 'option') ): ?>
        <?php while( have_rows('slider', 'option') ): the_row(); 
            $image = get_sub_field('image', 'option');
        ?>
        <a href="#" class="swiper-slide">
            <img src="<?php the_sub_field('image', 'option'); ?>" alt="img">
        </a>
        <?php endwhile;
        else: ?>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
        <a href="#" class="swiper-slide">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/slider.png" alt="img" class="">
        </a>
    <?php endif; ?>
    </div>
    <div class="swiper-button-next w-10 h-10 rounded-full hover:bg-primary-100"></div>
    <div class="swiper-button-prev w-10 h-10 rounded-full hover:bg-primary-100"></div>
    <div class="swiper-pagination"></div>
</div>