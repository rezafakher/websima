<div class="container mx-auto my-24 px-4 lg:px-0">
    <h3 class="text-3xl text-center mb-14">اخبار و مقالات</h3>
    <div class="grid grid-cols-1 lg:grid-cols-3 gap-4">
    <?php 
        if( get_field('sort_post', 'option') == 'date' ) { ?>
    <?php
            query_posts([
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 3,
            ]);
            while (have_posts()): the_post();
        ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"
                class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
                <?php the_post_thumbnail('', array("class" => 'mb-4')); ?>
                <div class="text-zinc-100 group-hover:text-zinc-200 text-sm">
                    <span><?php the_time('j') ?></span>
                    <span><?php the_time('F') ?></span>
                    <span><?php the_time('Y') ?></span>
                </div>
                <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                <?php
                    echo wp_trim_words(get_the_title(), 10, '...');
                ?>
                </div>
                <div class="text-zinc-300 group-hover:text-black leading-7 text-center">
                    <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?>
                </div>
            </a>
        <?php endwhile;
        wp_reset_query(); ?>
<?php } ?> 
<?php 
        if( get_field('sort_post', 'option') == 'category' ) { ?>
    <?php
            query_posts([
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 3,
            ]);
            while (have_posts()): the_post();
        ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"
                class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
                <?php the_post_thumbnail('', array("class" => 'mb-4')); ?>
                <div class="text-zinc-100 group-hover:text-zinc-200 text-sm">
                    <span><?php the_time('j') ?></span>
                    <span><?php the_time('F') ?></span>
                    <span><?php the_time('Y') ?></span>
                </div>
                <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                <?php
                    echo wp_trim_words(get_the_title(), 10, '...');
                ?>
                </div>
                <div class="text-zinc-300 group-hover:text-black leading-7 text-center">
                    <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?>
                </div>
            </a>
        <?php endwhile;
        wp_reset_query(); ?>
<?php } ?>   
<?php 
        if( get_field('sort_post', 'option') == 'custom' ) { ?>
    <?php
            query_posts([
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 3,
                'meta_key'			=> 'sort_custom',
                'orderby'			=> 'meta_value',
            ]);
            while (have_posts()): the_post();
        ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"
                class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
                <?php the_post_thumbnail('', array("class" => 'mb-4')); ?>
                <div class="text-zinc-100 group-hover:text-zinc-200 text-sm">
                    <span><?php the_time('j') ?></span>
                    <span><?php the_time('F') ?></span>
                    <span><?php the_time('Y') ?></span>
                </div>
                <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                <?php
                    echo wp_trim_words(get_the_title(), 10, '...');
                ?>
                </div>
                <div class="text-zinc-300 group-hover:text-black leading-7 text-center">
                    <?php echo wp_trim_words(get_the_excerpt(), 10, '...'); ?>
                </div>
            </a>
        <?php endwhile;
        wp_reset_query(); ?>
<?php } ?>   
    </div>
</div>
