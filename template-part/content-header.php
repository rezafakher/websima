<div class="container mx-auto px-4 lg:px-0">
    <div class="grid grid-cols-4 gap-4 items-center py-4 lg:py-0 header">
        <div class="flex justify-center lg:justify-start col-span-4 lg:col-span-1">
        <?php if( get_field('logo') ): ?>
            <img src="<?php the_field('logo', 'option'); ?>" alt="logo">
            <?php else: ?>
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/logo.png" alt="logo">
        <?php endif; ?>
        </div>
        <div class="col-span-2 lg:col-span-2 mt-4">
            <div class="block lg:hidden button-menu">
                <div class="w-12 h-1 bg-black"></div>
                <div class="w-12 h-1 bg-black my-2"></div>
                <div class="w-12 h-1 bg-black"></div>
            </div>
            <div class="justify-center items-center gap-8 hidden lg:flex flex-col lg:flex-row lg:w-auto w-full lg:relative absolute top-0 bottom-0 left-0 right-0 bg-white z-10 menu">
                <button type="button" class="bg-white rounded-md p-2 inline-flex lg:hidden items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500 close">
                <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
                </button>    
                <a href="#" class="hover:text-primary-100">صفحه اصلی</a>
                <div href="#" class="hover:text-primary-100 group relative cursor-pointer">
                    خدمات
                    <div class="hidden group-hover:flex flex-col absolute top-6 w-40 z-10 bg-white shadow-md gap-4 p-4 border-1 border-solid border-gray-100 rounded-md">
                        <a href="#">خدمات یک</a>
                        <a href="#">خدمات یک</a>
                        <a href="#">خدمات یک</a>
                        <a href="#">خدمات یک</a>
                    </div>
                </div>
                <a href="#" class="hover:text-primary-100">بلاگ</a>
                <a href="#" class="hover:text-primary-100">درباره ما</a>
                <a href="#" class="hover:text-primary-100">تماس با ما</a>
            </div>
        </div>
        <a href="<?php the_field('button_link', 'option'); ?>" class="col-span-2 lg:col-span-1 mt-4">
            <div class="bg-primary-100 flex justify-center items-center h-10 lg:w-32 w-full mr-auto rounded-md"><?php the_field('button_text', 'option'); ?></div>
        </a>  
    </div>
</div>
