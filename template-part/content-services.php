<div class="container mx-auto px-4 lg:px-0 lg:mt-20 mt-10">
    <div class="flex flex-col lg:flex-row">
        <h3 class="text-3xl lg:mb-14 mb-6">خدمات</h3>
        <div class="flex gap-2 lg:gap-4 mr-auto pb-4 lg:pb-0">
            <div class="hover:bg-primary-100 border-1 border-black border-solid text-black rounded-sm text-sm w-28 lg:w-36 h-10 flex justify-center items-center cursor-pointer">
                دسته بندی اول
            </div>
            <div class="hover:bg-primary-100 border-1 border-black border-solid text-black rounded-sm text-sm w-28 lg:w-36 h-10 flex justify-center items-center cursor-pointer">
                دسته بندی دوم
            </div>
            <div class="hover:bg-primary-100 border-1 border-black border-solid text-black rounded-sm text-sm w-28 lg:w-36 h-10 flex justify-center items-center cursor-pointer">
                دسته بندی سوم
            </div>
        </div>
    </div>
    <div class="grid grid-cols-1 lg:grid-cols-4 gap-4">
        <a href="#"
            class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/service.png" alt="logo" class="mb-4">
            <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                لورم ایپسوم متن ساختگی
            </div>
        </a>
        <a href="#"
            class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/service2.png" alt="logo" class="mb-4">
            <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                لورم ایپسوم متن ساختگی
            </div>
        </a>
        <a href="#"
            class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/service3.png" alt="logo" class="mb-4">
            <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                لورم ایپسوم متن ساختگی
            </div>
        </a>
        <a href="#"
            class="group flex flex-col rounded-md items-center border-1 border-solid border-slate-100 hover:border-primary-100 shadow-md p-4">
            <img src="<?php echo get_template_directory_uri(); ?>/asset/src/svgs/service4.png" alt="logo" class="mb-4">
            <div class="text-zinc-400 group-hover:text-primary-200 text-lg my-2">
                لورم ایپسوم متن ساختگی
            </div>
        </a>
    </div>
</div>