<?php get_header(); ?>

<?php
    get_template_part("template-part/content", "slider");
    get_template_part("template-part/content", "services");
    get_template_part("template-part/content", "blog");
    // get_template_part("template-part/content", "special-product");
    // get_template_part("template-part/content", "top-post");
    // get_template_part("template-part/content", "products");
    // get_template_part("template-part/content", "top-one-post");
?>

<?php get_footer(); ?>