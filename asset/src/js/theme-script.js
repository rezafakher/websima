import Swiper, {
    Navigation,
    Pagination
} from 'swiper';
Swiper.use([Navigation, Pagination]);

// Initialize Swiper
new Swiper(".mySwiper", {
    slidesPerView: 1,
    spaceBetween: 5,
    slidesPerGroup: 2,
    loopFillGroupWithBlank: true,
    loop: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});


String.prototype.convertDigit = function (conversion) {
    // Arabic chars : ٠١٢٣٤٥٦٧٨٩
    // Persian chars: ۰۱۲۳۴۵۶۷۸۹
    if (conversion == 'p2e') {
        return this.replace(/[٠-٩]/g, function (a) {
            return {
                '٠': '0',
                '١': '1',
                '٢': '2',
                '٣': '3',
                '٤': '4',
                '٥': '5',
                '٦': '6',
                '٧': '7',
                '٨': '8',
                '٩': '9'
            } [a];
        }).replace(/[۰-۹]/g, function (a) {
            return {
                '۰': '0',
                '۱': '1',
                '۲': '2',
                '۳': '3',
                '۴': '4',
                '۵': '5',
                '۶': '6',
                '۷': '7',
                '۸': '8',
                '۹': '9'
            } [a];
        });
    } else if (conversion == 'e2p')
        return this.replace(/\d/g, function (a) {
            return ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'][a];
        });
}

function showModal(title, content, footer) {
    if ($('.modal-backdrop').length != 0) {
        hideModal();
        setTimeout(function () {
            showModal(title, content, footer);
        }, 300);
        return;
    }
    if (!footer && footer !== null)
        footer = "<button type='button' class='btn btn-default' data-dismiss='modal'>بازگشت</button>";
    var modal = $("#modal");
    modal.find(".modal-header").find('.modal-title').html(title);
    modal.find(".modal-body").html(content);
    modal.find(".modal-footer").html(footer);

    if (footer === null)
        modal.find(".modal-footer").hide();
    else
        modal.find(".modal-footer").show();
    modal.modal('show');
}

function hideModal() {
    var modal = $("#modal");
    modal.modal('hide');
}

$('.button-menu').on('click',function(){
    $('.menu').addClass('show');
})

$('.close').on('click',function(){
    $('.menu').removeClass('show');
})