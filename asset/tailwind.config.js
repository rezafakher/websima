const colors = require('tailwindcss/colors')

module.exports = {
  darkMode: 'class',
  content: [
    // Paths to your templates...
    "../**.php",
    "../**/**.php",
    "./src/js/**.js"
  ],
  theme: {
    colors: {
      primary: {
        100: '#FECB0B',
        200: '#FFC900',
      },
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: {
        100: '#CACACA',
        200: '#787878',
        300: '#565656',
        400: '#191919',
        500: '#0F0F0F',
      },
      emerald: colors.emerald,
      indigo: colors.indigo,
      yellow: colors.yellow,
      slate: {
        100: '#EFEFEF',
      },
      zinc: {
        100: '#C4C4C4',
        200: '#242424',
        300: '#767676',
        400: '#3B3A39',
      },
      red: colors.red,
      orange: colors.emerald,
      lime: colors.lime,
      green: colors.green,
      blue: colors.blue,
      violet: colors.violet,
      purple: colors.purple,
      pink: colors.pink,
      rose: colors.rose,
    },
    extend: {
      fontFamily: {
        IRANYekan: ['"IRANYekan"', "serif"],
        Pushster: ['"Pushster"', "cursive"],
      },
      lineHeight: {
        '7': '30px',
      },
      boxShadow: {
        'md': '0 4px 10px 0 rgb(0 0 0 / 8%);',
      },
      borderWidth: {
        '1': '1px',
      },
      fontSize: {
        '3xl': '28px',
      }
    },
  },
  plugins: [
    require('tailwindcss-rtl'),
  ],
}
