<?php
add_action('wp_enqueue_scripts', 'websima_enqueue_scripts');
if ( ! function_exists( 'websima_enqueue_scripts' ) ) {
    /**
     * Enqueue theme styles and scripts
     */
    function websima_enqueue_scripts() {
        $template_url = get_template_directory_uri();
        // Styles
        wp_enqueue_style( 'websima_styles', $template_url . '/asset/build/css/main.css');
        // Scripts
        wp_deregister_script( 'jquery' );
        wp_enqueue_script( 'jquery', $template_url . '/asset/src/js/jquery.min.js',null,true );
        wp_enqueue_script( 'main', $template_url . '/asset/build/js/main.js', array(), false, true);
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {wp_enqueue_script( 'comment-reply' );}
    }
}

add_action('after_setup_theme', 'websima_setup_theme');
if ( ! function_exists( 'websima_setup_theme' ) ) {
    /**
     * Fireup the theme
     */
    function websima_setup_theme() {
        add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
    }
}

// Creating a services Custom Post Type
function crunchify_services_custom_post_type() {
	$labels = array(
		'name'                => __( 'services' ),
		'singular_name'       => __( 'service'),
		'menu_name'           => __( 'services'),
		'parent_item_colon'   => __( 'Parent service'),
		'all_items'           => __( 'All services'),
		'view_item'           => __( 'View service'),
		'add_new_item'        => __( 'Add New service'),
		'add_new'             => __( 'Add New'),
		'edit_item'           => __( 'Edit service'),
		'update_item'         => __( 'Update service'),
		'search_items'        => __( 'Search service'),
		'not_found'           => __( 'Not Found'),
		'not_found_in_trash'  => __( 'Not found in Trash')
	);
	$args = array(
		'label'               => __( 'services'),
		'description'         => __( 'Best Crunchify services'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields'),
		'public'              => true,
		'hierarchical'        => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'has_archive'         => true,
		'can_export'          => true,
		'exclude_from_search' => false,
	        'yarpp_support'       => true,
		'taxonomies' 	      => array('post_tag'),
		'publicly_queryable'  => true,
		'capability_type'     => 'page'
);
	register_post_type( 'services', $args );
}
add_action( 'init', 'crunchify_services_custom_post_type', 0 );

// Creating a Custom taxonomy
add_action( 'init', 'crunchify_create_services_custom_taxonomy', 0 );
 
function crunchify_create_services_custom_taxonomy() {
  $labels = array(
    'name' => _x( 'category-services', 'taxonomy general name' ),
    'singular_name' => _x( 'category-services', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search category-services' ),
    'all_items' => __( 'All category-services' ),
    'parent_item' => __( 'Parent category-services' ),
    'parent_item_colon' => __( 'Parent category-services:' ),
    'edit_item' => __( 'Edit category-services' ), 
    'update_item' => __( 'Update category-services' ),
    'add_new_item' => __( 'Add New category-services' ),
    'new_item_name' => __( 'New category-services Name' ),
    'menu_name' => __( 'category-services' ),
  ); 	
 
  register_taxonomy('catservices',array('services'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'type' ),
  ));
}

// acf_add_options_page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}